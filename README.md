# Simple nodejs api (KOA) performing both grpc unary and stream operation.

## How to run
The api should be run in conjunction with an GRPC endpoint, exposing  unary and stream operations. The application uses 
https://bitbucket.org/prashain/simple-grpc-api/src/master/ for service grpc operations. Clone this api and follow the readme to run the server.
The application exposes two commands to run the server
-   yarn watch [ runs nodemon]
-   yarn start

Both of the commands will start the server and listen to GRPC server on port 50051. The server itself listens on port 9000. You can issue curl command to invoke grpc userProfile operations
curl -v   http://localhost:9000/welcome\?user\=2

#### Generating proto stub
build-utils/scripts/protogen.sh
The above file will generate the proto stubs in src/proto directory.
