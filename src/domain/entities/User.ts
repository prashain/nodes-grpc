import { Entity, Column, PrimaryColumn } from "typeorm";
@Entity("user")
export class User {
  @PrimaryColumn()
  user_id: number;

  @Column()
  username: string;

  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @Column()
  email: string;
}
