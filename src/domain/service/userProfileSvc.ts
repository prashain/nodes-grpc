import { findByID } from "../../utils/repository/userRepository";

export const findUserById = async (userId: number) => {
  console.log(`Fetching mocked up user with id ${userId}`);
  return findByID(userId);
};
