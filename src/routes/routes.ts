import Router from "koa-router";
import { PassThrough } from "stream";
import { User } from "../domain/entities/User";
import { findUserById } from "../domain/service/userProfileSvc";
import client from "../grpc/client";
import { StreamRequest, UserProfileResponse } from "../proto/usersvc_pb";
import { saveUser } from "../utils/repository/userRepository";

const userRoutes = new Router();

userRoutes.get("/user/:userId", async (ctx, next) => {
  console.log(ctx.request);
  const userId = ctx.params.userId;
  console.log(`The userId from path is ${userId}`);
  const user = await findUserById(userId);
  if (user) {
    ctx.body = {
      status: 200,
      data: user,
      message: "comll",
    };
  } else {
    ctx.body = {
      status: 400,
    };
  }
  await next();
});

userRoutes.post("/user", async (ctx, next) => {
  console.log(ctx.request);
  const reqBody = ctx.request.body;
  console.log(`The userIs from path is ${reqBody}`);
  await saveUser(reqBody);
  ctx.status = 201;

  await next();
});

// userRoutes.get("/users", async (ctx,next) => {

//    getUsers();
//    const users= await findAll()
//    if(users){
//     ctx.body = {
//       status: 200,
//       data: users,
//       message: "comll",
//     };
//    }
// })

userRoutes.get("/users", async (ctx, next) => {
  const req = new StreamRequest();
  const stream = client.fetchUsers(req);

  ctx.set("Content-Type", "application/x-ndjson");
  ctx.set("Transfer-Encoding", "chunked");
  const koa_Stream = new PassThrough();
  ctx.status = 200;
  ctx.body = koa_Stream;
  if (stream) {
    stream.on("data",  (value: UserProfileResponse) => {
      console.log(`The user from grpc is ${value}`);
       koa_Stream.write(`data: ${JSON.stringify(mapResponseToUser(value))}\n\n`);
    });
    stream.on("close", () => {
      koa_Stream.end();
    });
  }
  await next();
});

const mapResponseToUser = (from :UserProfileResponse) : User => {
  return {
    user_id : from.getUserId(),
    username : from.getUsername(),
    first_name : from.getFirstName(),
    last_name : from.getLastName(),
    email: from.getEmail()

  }
}

export { userRoutes };
