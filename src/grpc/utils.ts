import { StreamRequest, UserProfileRequest, UserProfileResponse } from '../proto/usersvc_pb';
import client from "./client";
export const getUser = async (userId:number) => {
    const userDTO:UserProfileRequest = new UserProfileRequest()
    userDTO.setUserid(userId);
     client.fetchUser(userDTO,(e,y) => {
        console.log("grpc " + y["array"])
        return y.getUsername
     })
}

export function getUserv2(userId){
    return new Promise<UserProfileResponse>((resolve, reject) => {
        const val:UserProfileRequest = new UserProfileRequest()
        val.setUserid(userId);
        client.fetchUser(val, (err, userResponse) => {
            if (err) {
                console.log(`Error obtaining user profile for id ${userId}`)
                console.log(`${err.message}}`)
                return reject(err);
            }
            return resolve(userResponse);
        });
    });
}
export function getUsers(){
    const req = new StreamRequest()
    let stream = client.fetchUsers(req);
    stream.on('data',(user:UserProfileResponse)=>{
        console.log(`Received ${user.getUsername()} from the server`);
        

    });
    stream.on('end',()=>{
        console.log("Finished receiving data from the server")

    })
    stream.on("erroor",(error) => {
        console.log(`Encountered error  while fetching data  ${error}`)
    })
}