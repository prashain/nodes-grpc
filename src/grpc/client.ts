import * as grpc from '@grpc/grpc-js';
import { FetchUserProfileClient } from '../proto/usersvc_grpc_pb';


export default new FetchUserProfileClient(
    "localhost:50051",
    grpc.credentials.createInsecure()
)

