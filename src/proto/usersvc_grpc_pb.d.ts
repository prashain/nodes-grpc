// package: codelabs.pragmatists.api.gencode
// file: usersvc.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as usersvc_pb from "./usersvc_pb";

interface IFetchUserProfileService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    fetchUser: IFetchUserProfileService_IFetchUser;
    fetchUsers: IFetchUserProfileService_IFetchUsers;
}

interface IFetchUserProfileService_IFetchUser extends grpc.MethodDefinition<usersvc_pb.UserProfileRequest, usersvc_pb.UserProfileResponse> {
    path: "/codelabs.pragmatists.api.gencode.FetchUserProfile/FetchUser";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<usersvc_pb.UserProfileRequest>;
    requestDeserialize: grpc.deserialize<usersvc_pb.UserProfileRequest>;
    responseSerialize: grpc.serialize<usersvc_pb.UserProfileResponse>;
    responseDeserialize: grpc.deserialize<usersvc_pb.UserProfileResponse>;
}
interface IFetchUserProfileService_IFetchUsers extends grpc.MethodDefinition<usersvc_pb.StreamRequest, usersvc_pb.UserProfileResponse> {
    path: "/codelabs.pragmatists.api.gencode.FetchUserProfile/FetchUsers";
    requestStream: false;
    responseStream: true;
    requestSerialize: grpc.serialize<usersvc_pb.StreamRequest>;
    requestDeserialize: grpc.deserialize<usersvc_pb.StreamRequest>;
    responseSerialize: grpc.serialize<usersvc_pb.UserProfileResponse>;
    responseDeserialize: grpc.deserialize<usersvc_pb.UserProfileResponse>;
}

export const FetchUserProfileService: IFetchUserProfileService;

export interface IFetchUserProfileServer extends grpc.UntypedServiceImplementation {
    fetchUser: grpc.handleUnaryCall<usersvc_pb.UserProfileRequest, usersvc_pb.UserProfileResponse>;
    fetchUsers: grpc.handleServerStreamingCall<usersvc_pb.StreamRequest, usersvc_pb.UserProfileResponse>;
}

export interface IFetchUserProfileClient {
    fetchUser(request: usersvc_pb.UserProfileRequest, callback: (error: grpc.ServiceError | null, response: usersvc_pb.UserProfileResponse) => void): grpc.ClientUnaryCall;
    fetchUser(request: usersvc_pb.UserProfileRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: usersvc_pb.UserProfileResponse) => void): grpc.ClientUnaryCall;
    fetchUser(request: usersvc_pb.UserProfileRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: usersvc_pb.UserProfileResponse) => void): grpc.ClientUnaryCall;
    fetchUsers(request: usersvc_pb.StreamRequest, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<usersvc_pb.UserProfileResponse>;
    fetchUsers(request: usersvc_pb.StreamRequest, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<usersvc_pb.UserProfileResponse>;
}

export class FetchUserProfileClient extends grpc.Client implements IFetchUserProfileClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public fetchUser(request: usersvc_pb.UserProfileRequest, callback: (error: grpc.ServiceError | null, response: usersvc_pb.UserProfileResponse) => void): grpc.ClientUnaryCall;
    public fetchUser(request: usersvc_pb.UserProfileRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: usersvc_pb.UserProfileResponse) => void): grpc.ClientUnaryCall;
    public fetchUser(request: usersvc_pb.UserProfileRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: usersvc_pb.UserProfileResponse) => void): grpc.ClientUnaryCall;
    public fetchUsers(request: usersvc_pb.StreamRequest, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<usersvc_pb.UserProfileResponse>;
    public fetchUsers(request: usersvc_pb.StreamRequest, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<usersvc_pb.UserProfileResponse>;
}
