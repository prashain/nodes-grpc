// package: codelabs.pragmatists.api.gencode
// file: usersvc.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class UserProfileRequest extends jspb.Message { 
    getUserid(): number;
    setUserid(value: number): UserProfileRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UserProfileRequest.AsObject;
    static toObject(includeInstance: boolean, msg: UserProfileRequest): UserProfileRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UserProfileRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UserProfileRequest;
    static deserializeBinaryFromReader(message: UserProfileRequest, reader: jspb.BinaryReader): UserProfileRequest;
}

export namespace UserProfileRequest {
    export type AsObject = {
        userid: number,
    }
}

export class UserProfileResponse extends jspb.Message { 
    getUserId(): number;
    setUserId(value: number): UserProfileResponse;
    getUsername(): string;
    setUsername(value: string): UserProfileResponse;
    getFirstName(): string;
    setFirstName(value: string): UserProfileResponse;
    getLastName(): string;
    setLastName(value: string): UserProfileResponse;
    getEmail(): string;
    setEmail(value: string): UserProfileResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UserProfileResponse.AsObject;
    static toObject(includeInstance: boolean, msg: UserProfileResponse): UserProfileResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UserProfileResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UserProfileResponse;
    static deserializeBinaryFromReader(message: UserProfileResponse, reader: jspb.BinaryReader): UserProfileResponse;
}

export namespace UserProfileResponse {
    export type AsObject = {
        userId: number,
        username: string,
        firstName: string,
        lastName: string,
        email: string,
    }
}

export class StreamRequest extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StreamRequest.AsObject;
    static toObject(includeInstance: boolean, msg: StreamRequest): StreamRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StreamRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StreamRequest;
    static deserializeBinaryFromReader(message: StreamRequest, reader: jspb.BinaryReader): StreamRequest;
}

export namespace StreamRequest {
    export type AsObject = {
    }
}
