// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var usersvc_pb = require('./usersvc_pb.js');

function serialize_codelabs_pragmatists_api_gencode_StreamRequest(arg) {
  if (!(arg instanceof usersvc_pb.StreamRequest)) {
    throw new Error('Expected argument of type codelabs.pragmatists.api.gencode.StreamRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_codelabs_pragmatists_api_gencode_StreamRequest(buffer_arg) {
  return usersvc_pb.StreamRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_codelabs_pragmatists_api_gencode_UserProfileRequest(arg) {
  if (!(arg instanceof usersvc_pb.UserProfileRequest)) {
    throw new Error('Expected argument of type codelabs.pragmatists.api.gencode.UserProfileRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_codelabs_pragmatists_api_gencode_UserProfileRequest(buffer_arg) {
  return usersvc_pb.UserProfileRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_codelabs_pragmatists_api_gencode_UserProfileResponse(arg) {
  if (!(arg instanceof usersvc_pb.UserProfileResponse)) {
    throw new Error('Expected argument of type codelabs.pragmatists.api.gencode.UserProfileResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_codelabs_pragmatists_api_gencode_UserProfileResponse(buffer_arg) {
  return usersvc_pb.UserProfileResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var FetchUserProfileService = exports.FetchUserProfileService = {
  fetchUser: {
    path: '/codelabs.pragmatists.api.gencode.FetchUserProfile/FetchUser',
    requestStream: false,
    responseStream: false,
    requestType: usersvc_pb.UserProfileRequest,
    responseType: usersvc_pb.UserProfileResponse,
    requestSerialize: serialize_codelabs_pragmatists_api_gencode_UserProfileRequest,
    requestDeserialize: deserialize_codelabs_pragmatists_api_gencode_UserProfileRequest,
    responseSerialize: serialize_codelabs_pragmatists_api_gencode_UserProfileResponse,
    responseDeserialize: deserialize_codelabs_pragmatists_api_gencode_UserProfileResponse,
  },
  fetchUsers: {
    path: '/codelabs.pragmatists.api.gencode.FetchUserProfile/FetchUsers',
    requestStream: false,
    responseStream: true,
    requestType: usersvc_pb.StreamRequest,
    responseType: usersvc_pb.UserProfileResponse,
    requestSerialize: serialize_codelabs_pragmatists_api_gencode_StreamRequest,
    requestDeserialize: deserialize_codelabs_pragmatists_api_gencode_StreamRequest,
    responseSerialize: serialize_codelabs_pragmatists_api_gencode_UserProfileResponse,
    responseDeserialize: deserialize_codelabs_pragmatists_api_gencode_UserProfileResponse,
  },
};

exports.FetchUserProfileClient = grpc.makeGenericClientConstructor(FetchUserProfileService);
