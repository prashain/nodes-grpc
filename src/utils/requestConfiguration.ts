import { AxiosRequestConfig } from "axios";

export const requestConfiguration = (): AxiosRequestConfig => {
  const defaultHeaders = {
    Accept: "application/json",
    "Content-Type": "application/json",
    "X-Content-Type-Options": "nosniff",
    "Cache-Control": "no-cache, no-store, must-revalidate",
    Pragma: "no-cache",
    Expires: "0"
  };

  return { headers: defaultHeaders };
};