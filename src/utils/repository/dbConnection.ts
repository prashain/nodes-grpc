import path from "path"
import {  createConnection } from "typeorm"

export const  createDBConn =  async () => {
    console.log("Creating connection" + __dirname )
    path.join(__dirname)

    return  createConnection({
        type: "mysql",
        host: "localhost",
        port: 3306,
        username: "root",
        password: "admin",
        database: "cbo",
        entities: [
            __dirname + "../../domain/entities/*.ts"
        ],
        logging: true,
        insecureAuth:true
      })
}