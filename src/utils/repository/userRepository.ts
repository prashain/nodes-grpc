import { getConnection } from "typeorm";
import { User } from "../../domain/entities/User";

export const findByID = async (userId: number) => {
  return getConnection().getRepository(User).findOne(userId);
};

export const saveUser = async (input:User) => {
    return getConnection().getRepository(User).save(input)
}

export const findAll = async () => {
  return getConnection().getRepository(User).find();
};