import axios, { AxiosResponse } from "axios";
import { SERVICE_BASE_URL } from "./constants";
import { requestConfiguration } from "./requestConfiguration";

const getRequest = (path: string): any => {
    return axios.get(SERVICE_BASE_URL + path, requestConfiguration());
  };
  
  const postRequest = (
    url = SERVICE_BASE_URL,
    path: string,
    payload: {}
  ): Promise<AxiosResponse<any>> => {
    return axios.post(url + path, payload, requestConfiguration());
  };

  export const httpRequestHandler = { getRequest, postRequest };