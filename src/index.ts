import Koa from "koa";
import bodyParser from "koa-bodyparser";
import Router from "koa-router";
import { createConnection } from "typeorm";
import { getUserv2 } from "./grpc/utils";
import { userRoutes } from "./routes/routes";
import { IUser } from "./domain/models";

createConnection({
  type: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "admin",
  database: "cbo",
  entities: [__dirname + "/domain/entities/*.ts"],
  logging: true,
  insecureAuth: true,
})
  .then(async (conn) => {
    const app = new Koa();
    app.use(bodyParser());

    const router = new Router();

    router.get("/welcome", async (ctx, next) => {
      const queryString = ctx.request.query;
      console.log(`The user from querystring is ${queryString.user}`);
      const id = queryString.user ? queryString.user : 1;
      try {
        const grpcReponse: IUser = await getUserv2(id).then((val) => {
          console.log("The value coming is" + val);
          return {
            email: val.getEmail(),
            first_name: val.getFirstName(),
            last_name: val.getLastName(),
            username: val.getUsername(),
            id: val.getUserId(),
          };
        });
        ctx.body = {
          message: `Welcome ${grpcReponse.first_name} ${grpcReponse.last_name}`,
        };
      } catch (error: any) {
        console.log(`Error while getting user profile ${error.message}`);
        ctx.body = {
          error: `Cannot retrieve user for ${id}`,
          code: 404,
        };
        ctx.status = 404;
      }
      await next();
    });

    router.get("/test", async (ctx, next) => {
      console.log(ctx.request);
      ctx.body = {
        message: "welcome",
      };
      await next();
    });

    app.use(router.routes()).use(router.allowedMethods());
    app.use(userRoutes.routes());

    app.use(async (ctx, next) => {
      ctx.set("Access-Control-Allow-Origin", "*");
      ctx.set(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
      );
      ctx.set(
        "Access-Control-Allow-Methods",
        "POST, GET, PUT, DELETE, OPTIONS"
      );
      await next();
    });

    app.listen(9000, () => {
      console.log("server started");
    });
  })
  .catch((error) => {
    console.log(`Error while creating connection ${error}`);
  });
