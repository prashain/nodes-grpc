"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.requestConfiguration = void 0;
const requestConfiguration = () => {
    const defaultHeaders = {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Content-Type-Options": "nosniff",
        "Cache-Control": "no-cache, no-store, must-revalidate",
        Pragma: "no-cache",
        Expires: "0"
    };
    return { headers: defaultHeaders };
};
exports.requestConfiguration = requestConfiguration;
//# sourceMappingURL=requestConfiguration.js.map