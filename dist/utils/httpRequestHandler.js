"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.httpRequestHandler = void 0;
const axios_1 = __importDefault(require("axios"));
const constants_1 = require("./constants");
const requestConfiguration_1 = require("./requestConfiguration");
const getRequest = (path) => {
    return axios_1.default.get(constants_1.SERVICE_BASE_URL + path, requestConfiguration_1.requestConfiguration());
};
const postRequest = (url = constants_1.SERVICE_BASE_URL, path, payload) => {
    return axios_1.default.post(url + path, payload, requestConfiguration_1.requestConfiguration());
};
exports.httpRequestHandler = { getRequest, postRequest };
//# sourceMappingURL=httpRequestHandler.js.map