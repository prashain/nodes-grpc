"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_1 = __importDefault(require("koa"));
const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
const koa_router_1 = __importDefault(require("koa-router"));
const typeorm_1 = require("typeorm");
const utils_1 = require("./grpc/utils");
const routes_1 = require("./routes/routes");
typeorm_1.createConnection({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "admin",
    database: "cbo",
    entities: [__dirname + "/domain/entities/*.ts"],
    logging: true,
    insecureAuth: true,
})
    .then((conn) => __awaiter(void 0, void 0, void 0, function* () {
    const app = new koa_1.default();
    app.use(koa_bodyparser_1.default());
    const router = new koa_router_1.default();
    router.get("/welcome", (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
        const queryString = ctx.request.query;
        console.log(`The user from querystring is ${queryString.user}`);
        const id = queryString.user ? queryString.user : 1;
        const grpcReponse = yield utils_1.getUser1(id).then(val => {
            console.log("The value coming is" + val);
            return val;
        });
        console.log(grpcReponse.getEmail());
        ctx.body = {
            message: `Welcome ${grpcReponse.getFirstName()} ${grpcReponse.getLastName()}`
        };
        // await httpRequestHandler.getRequest(`/users/${id}`).then(
        //   (res: any) => {
        //     console.log(res.data);
        //     ctx.body = { message: `Welcome ${res.data.first_name} ${res.data.last_name}`};
        //   },
        //   (err: any) => {
        //     console.log(`err.res : ${JSON.stringify(err)}`);
        //     ctx.body = {
        //       message: err.message,
        //       statusCode: err.response.status,
        //     };
        //   }
        // );
        yield next();
    }));
    router.get("/test", (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
        console.log(ctx.request);
        ctx.body = {
            message: "welcome",
        };
        yield next();
    }));
    app.use(router.routes()).use(router.allowedMethods());
    app.use(routes_1.userRoutes.routes());
    app.use((ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
        ctx.set("Access-Control-Allow-Origin", "*");
        ctx.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        ctx.set("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        yield next();
    }));
    app.listen(9000, () => {
        console.log("server started");
    });
}))
    .catch((error) => {
    console.log(`Error while creating connection ${error}`);
});
//# sourceMappingURL=index.js.map