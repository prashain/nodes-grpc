"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRoutes = void 0;
const koa_router_1 = __importDefault(require("koa-router"));
const userProfileSvc_1 = require("../domain/service/userProfileSvc");
const userRepository_1 = require("../utils/repository/userRepository");
const userRoutes = new koa_router_1.default();
exports.userRoutes = userRoutes;
userRoutes.get("/user/:userId", (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(ctx.request);
    const userId = ctx.params.userId;
    console.log(`The userId from path is ${userId}`);
    const user = yield userProfileSvc_1.findUserById(userId);
    if (user) {
        ctx.body = {
            status: 200,
            data: user,
            message: "comll",
        };
    }
    else {
        ctx.body = {
            status: 400,
        };
    }
    yield next();
}));
userRoutes.post("/user", (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(ctx.request);
    const reqBody = ctx.request.body;
    console.log(`The userIs from path is ${reqBody}`);
    yield userRepository_1.saveUser(reqBody);
    ctx.status = 201;
    yield next();
}));
//# sourceMappingURL=routes.js.map