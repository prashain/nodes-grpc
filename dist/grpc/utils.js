"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUser1 = exports.getUser = void 0;
const usersvc_pb_1 = require("../proto/usersvc_pb");
const client_1 = __importDefault(require("./client"));
const getUser = (userId) => __awaiter(void 0, void 0, void 0, function* () {
    const val = new usersvc_pb_1.UserProfileRequest();
    val.setUserid(userId);
    client_1.default.fetchUser(val, (e, y) => {
        console.log("grpc " + y["array"]);
        return y.getUsername;
    });
});
exports.getUser = getUser;
function getUser1(userId) {
    return new Promise((resolve, reject) => {
        const val = new usersvc_pb_1.UserProfileRequest();
        val.setUserid(userId);
        client_1.default.fetchUser(val, (err, userResponse) => {
            if (err) {
                console.log("caugh error");
                return reject(err);
            }
            return resolve(userResponse);
        });
    });
}
exports.getUser1 = getUser1;
//# sourceMappingURL=utils.js.map